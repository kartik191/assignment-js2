function arrayToString(array){
    if(!Array.isArray(array)){
        return;
    }
    let string = array.join(' ');
    const regex = /\s+/g;
    string = string.replaceAll(regex, ' ');
    return string.trim();
}

module.exports = { arrayToString };