function ipToNumberArray(string){
    if(typeof(string) !== 'string'){
        return [];
    }
    let array = string.split('.');  
    if(array.length !== 4) return [];
    for(let i = 0; i < array.length; i++){
        let num = Number(array[i]);
        if(array[i].length > 0 && (num >= 0 && num <= 255)){
            array[i] =  num;
        }
        else {
            return [];
        }
    }
    return array;
}

module.exports = { ipToNumberArray };