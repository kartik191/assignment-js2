function getMonth(string){
    const reg = new RegExp('^([0-9]{1,2}/){2}[0-9]{4}$');
    if(typeof(string) !== 'string' || !reg.test(string)){
        return;
    }
    const mm = Number(string.split('/')[0]);
    const months = ['','January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    if(mm >= 1 && mm <= 12){
        return months[mm];
    }
}

module.exports = { getMonth };