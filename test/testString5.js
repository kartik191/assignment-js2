const string5 = require('../string5');

let array = ["the", "quick", "brown", "fox"];
let string = string5.arrayToString(array);
console.log(string);

array = ["the", "quick", 235, "fox"];
string = string5.arrayToString(array);
console.log(string);

array = ["  the", null, 235, "fox", undefined, "jumb  "];
string = string5.arrayToString(array);
console.log(string);

string = string5.arrayToString();
console.log(string);

string = string5.arrayToString('asdfgh');
console.log(string);

array = ["the", 235, "fox", undefined];
string = string5.arrayToString(array);
console.log(string);

string = string5.arrayToString([]);
console.log(string);